package com.aptonline.repository;

import java.util.List;

import com.aptonline.model.Book;

public interface BookRepository {

	Book save(Book book);

	List<Book> findAllBooks();
}
