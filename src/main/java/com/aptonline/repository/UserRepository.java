package com.aptonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptonline.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

}
