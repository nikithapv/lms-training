package com.aptonline.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.aptonline.model.Book;

@Repository("bookRepository")
public class BookRepositoryImpl implements BookRepository {
	
	@PersistenceContext
	private EntityManager em;
	

	public Book save(Book book) {
		
		em.persist(book);
		em.flush();
		
		return book;
	}


	
	@SuppressWarnings({"unchecked","rawtypes"})
	public List<Book> findAllBooks() {
		Query query=em.createQuery("Select b from Book b");
		List books= query.getResultList();
		return books;
	}

}
