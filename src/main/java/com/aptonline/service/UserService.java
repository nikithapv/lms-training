package com.aptonline.service;

import com.aptonline.model.User;

public interface UserService {

	User save(User user);
	
}
