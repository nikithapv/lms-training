package com.aptonline.service;

import java.util.List;

import com.aptonline.model.Book;

public interface BookService {

	Book save(Book book);

	List<Book> findAllBooks();
}
