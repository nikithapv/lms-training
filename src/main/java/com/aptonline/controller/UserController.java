package com.aptonline.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aptonline.model.User;
import com.aptonline.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "newUser", method = RequestMethod.GET)
	public String newUser(@ModelAttribute("userForm") User user) {
		System.out.println("User==");
		return "newUser";
	}
	
	@RequestMapping(value = "newUser", method = RequestMethod.POST)
	public String insertUser(@Valid @ModelAttribute("userForm") User user, BindingResult result) {
		
		if (result.hasErrors()) {
			return "newUser";
		} else {
			userService.save(user);
		}
		
		return "redirect:index.jsp";
	}
}
