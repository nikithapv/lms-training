package com.aptonline.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aptonline.model.Book;
import com.aptonline.service.BookService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class BookController {

	@Autowired
	private BookService bookService;
	
	private static final Logger logger = LoggerFactory.getLogger(BookController.class);

	/*
	 * @RequestMapping(value = "/", method = RequestMethod.GET) public String
	 * home(Locale locale, Model model) {
	 * logger.info("Welcome home! The client locale is {}.", locale);
	 * 
	 * Date date = new Date(); DateFormat dateFormat =
	 * DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
	 * 
	 * String formattedDate = dateFormat.format(date);
	 * 
	 * model.addAttribute("serverTime", formattedDate );
	 * 
	 * return "home"; }
	 */

	@RequestMapping(value = "addBook", method = RequestMethod.GET)
	public String addBook(@ModelAttribute("book") Book book) {
		System.out.println("====nikitha=======");
		return "addBook";
	}

	@RequestMapping(value = "addBook", method = RequestMethod.POST)
	public String insertBook(@Valid @ModelAttribute("book") Book book, BindingResult result) {

		if (result.hasErrors()) {
			return "addBook";
		} else {
			bookService.save(book);
		}

		return "redirect:index.jsp";
	}
	
	@RequestMapping(value = "viewBook", method = RequestMethod.GET)
	public String getBook(Model model) {
		List<Book> booksList=bookService.findAllBooks();
		model.addAttribute("books", booksList);
		return "viewBook";
	}
}
