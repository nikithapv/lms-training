package com.aptonline.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		System.out.println("In the Login Controller");
		return "login";
	}
	
	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginFailed(ModelMap model) {
		System.out.println("In the Login Controller failed method");
		model.addAttribute("error", true);
		return "login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		System.out.println("In the Login Controller Logut method");
		return "login";
	}
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String errorPage(ModelMap model) {
		System.out.println("In the Login Controller Error method");
		return "403";
	}
	
}
