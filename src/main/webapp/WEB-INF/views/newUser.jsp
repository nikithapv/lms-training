<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>


<!DOCTYPE html>
<html lang="en">

<head>
<title>Library</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<body>

	<div class="container">
		<div>
			<h1>User Enrollment</h1>
		</div>

		<form:form commandName="userForm"  method="POST" class="form-horizontal">
			<form:errors path="*" cssClass="errorblock" element="div" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="username">UserName:</label>
				<div class="col-sm-10">
					<form:input path="username"/>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="password">Password:</label>
				<div class="col-sm-10">
					<form:input path="password"/>
				</div>
			</div>
			<form:hidden path="enabled" value="1" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="usertype">User
					Type:</label>
				<div class="col-sm-10">
					<form:select path="usertype">
						<form:option value=""> --SELECT--</form:option>
						<form:option value="Student">Student</form:option>
						<form:option value="others">Others</form:option>
					</form:select>
					<form:errors path="usertype" cssClass="error" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn" value="SUBMIT" />
				</div>
			</div>

		</form:form>

		<div class="control-group"></div>
	</div>


</body>
</html>
