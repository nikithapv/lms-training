<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Library</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style type="text/css">
.errorblock {
	color: #ff0000;
	blackground: #ffEEEE;
	padding: 8px;
	margin: 16px
}

body {
	background: #333;
}

.form_bg {
	background-color: #eee;
	color: #666;
	padding: 20px;
	border-radius: 10px;
	position: absolute;
	border: 1px solid #fff;
	margin: auto;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	width: 320px;
	height: 280px;
}

.align-center {
	text-align: center;
}
</style>
</head>
<body onload="document.f.j_username.focus()">
	<div class="container">
		<div class="row">
			<div class="form_bg">
				<form method="POST" action="j_spring_security_check" name="f">
					<h2 class="text-center">Library Login</h2>
					<c:if test="${not empty error }">
						<div class="errorblock">
							login was unsuccessful<br>
							caused:${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
						</div>
					</c:if>
					<br />
					<div class="form-group">
						<input type="text" class="form-control" id="userid"
							name="j_username" placeholder="User id">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="pwd"
							name="j_password" placeholder="Password">
					</div>
					<br />
					<div class="align-center">
						<button type="submit" class="btn btn-default" id="login">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>