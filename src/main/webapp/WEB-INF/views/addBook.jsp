<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html lang="en">

<head>
<title>Library</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Library</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Book<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="addBook.html">Book Enrollment</a></li>
						<li><a href="viewBook.html">View</a></li>
					</ul></li>
				<!--  <li><a href="#">Page 2</a></li> -->
			</ul>
			<!-- <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul> -->
		</div>
	</nav>
	<div class="container">
		<div>
			<h1>Book Enrollment</h1>
			<p>
				Add your Books. <br> &nbsp;
			</p>
		</div>

		<form:form commandName="book" class="form-horizontal">
			<form:errors path="*" cssClass="errorblock" element="div" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="bookName">BookName:</label>
				<div class="col-sm-10">
					<form:input path="bookName" cssErrorClass="error" />
					<form:errors path="bookName" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="author">Author:</label>
				<div class="col-sm-10">
					<form:input path="author" cssErrorClass="error" />
					<form:errors path="author" cssClass="error" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="noOfBooks">BookName:</label>
				<div class="col-sm-10">
					<form:input path="noOfBooks" cssErrorClass="error" />
					<form:errors path="noOfBooks" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn" value="Enter Books" />
				</div>
			</div>

		</form:form>

		<div class="control-group"></div>
	</div>


</body>
</html>
